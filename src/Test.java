import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Test {
    public static void printList(ArrayList list) {
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + ": " + list.get(i));
        }
    }

    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add("Ivanoff");
        list.add("Sidoroff");
        System.out.println(list.size());
        System.out.println(list.get(0));

        list.add(0, "Petrosian");
        System.out.println(list.get(0));
        printList(list);

        list.set(1, "IVANOFF");
        printList(list);

        list.remove(1);
        printList(list);

        int n = list.indexOf("x");
        System.out.println(n);

        n = list.indexOf("Sidoroff");
        System.out.println(n);

        Iterator iter = list.iterator();
        System.out.println();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        list.add("Petrosian");
        printList(list);
        int count = Collections.frequency(list, "Petrosian");
        System.out.println(count);

        String[] str = (String[])list.toArray(new String[list.size()]);
        for (String s : str) {
            System.out.println(s);
        }

        ArrayList<String> listStr = new ArrayList<>();
        listStr.add("str111");
        listStr.add("str222");
//		list2.add(333);
        for (String s : listStr) {
            System.out.println(s);
        }

        ArrayList<Integer> listInt = new ArrayList<>();
        listInt.add(100);
        listInt.add(200);
        for (Integer i : listInt) {
            System.out.println(i);
        }
    }
}
